﻿using EmployeeProfile.DTOs;
namespace EmployeeProfile.Utilities
{
    public class EmployeeMessage
    {
        public bool create { get; set; }
        public EmployeeDTO EmployeeDto { get; set; }
    }
}
